extern crate rand;
extern crate gnuplot;
extern crate perfcnt;
#[macro_use]
extern crate maplit;

use std::collections::HashMap;

use rand::{Rng, weak_rng};
use rand::distributions::{IndependentSample, Range};

use gnuplot::{Figure, Caption, Color, AxesCommon, PlotOption, PointSymbol};

use perfcnt::{AbstractPerfCounter, PerfCounter};
use perfcnt::linux::{HardwareEventType, PerfCounterBuilderLinux, CacheId, CacheOpId, CacheOpResultId};

// size of usize -- the type in the values vec
const VALUE_SIZE: usize = 8;

// 4 gigs
const MAX_SIZE: usize = 4 * 1024 * 1024 * 1024 / VALUE_SIZE;
const ITERATIONS: usize = 10_000_000;

fn main() {
    // the sizes are designed for 64-bit systems
    assert_eq!(8, std::mem::size_of::<usize>());

    // we only need enough randomness to fool the prefetcher, not a CSPRNG
    let mut r = weak_rng();

    let mut results = Vec::new();

    let mut vec: Vec<usize> = Vec::with_capacity(MAX_SIZE);

    let mut counters: HashMap<&str, PerfCounter> = hashmap! {
        "Cycles" => PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::RefCPUCycles),
        "Instr" => PerfCounterBuilderLinux::from_hardware_event(HardwareEventType::Instructions),
        "L1 misses" => PerfCounterBuilderLinux::from_cache_event(CacheId::L1D, CacheOpId::Read, CacheOpResultId::Miss),
        "L1 hits" => PerfCounterBuilderLinux::from_cache_event(CacheId::L1D, CacheOpId::Read, CacheOpResultId::Access),
        "LLC misses" => PerfCounterBuilderLinux::from_cache_event(CacheId::LL, CacheOpId::Read, CacheOpResultId::Miss),
        "LLC hits" => PerfCounterBuilderLinux::from_cache_event(CacheId::LL, CacheOpId::Read, CacheOpResultId::Access),
        "DTLB misses" => PerfCounterBuilderLinux::from_cache_event(CacheId::DTLB, CacheOpId::Read, CacheOpResultId::Miss),
    }.into_iter()
        .map(|e| (e.0, e.1.finish().expect("Could not create counter")))
        .collect();

    let mut size = 8;
    while size <= MAX_SIZE {
        vec.clear();
        let range = Range::new(0, size);

        // fill with random garbage
        for _ in 1..(size + 1) {
            vec.push(range.ind_sample(&mut r));
        };

        for ref c in counters.values() {
            c.start().expect("Could not start counter");
        };

        let mut index = 0;
        for _ in 0..ITERATIONS {
            // use the random value as the next index
            index = vec[index];
        };

        for ref c in counters.values() {
            c.stop().expect("Could not stop counter");
        };

        let counter_data: HashMap<String, f64> = counters.iter_mut()
            .map(|e| (String::from(*e.0), e.1.read().expect("Could not read counter") as f64))
            .collect();

        println!("Size {} complete", size * VALUE_SIZE);
        for (name, count) in &counter_data {
            println!(" - {} = {}", name, count / (ITERATIONS as f64));
        }

        results.push(Datapoint {
            working_set_size: size * VALUE_SIZE,
            counter_data: counter_data
        });


        size = size.saturating_mul(2);
    };

    let mut fig = Figure::new();

    {
        let mut axes = fig
            .axes2d()
            .set_x_label("Power of 2 of working set size", &[])
            .set_y_label("Things per increment", &[])
            .set_y_log(Some(10.0));

        for &name in counters.keys() {
            axes.lines_points(sizes_iter(&results),
                              results.iter().map(|r| r.counter_data[name] / (ITERATIONS as f64)),
                              &[Caption(name), Color(&random_color(&mut r)), random_point_symbol(&mut r)]);
        };
    };

    fig.show();
}

fn random_color<R: Rng>(r: &mut R) -> String {
    let min = 50;
    let max = 200;

    format!("#{}{}{}", r.gen_range(min, max), r.gen_range(min, max), r.gen_range(min, max))
}

fn random_point_symbol<R: Rng>(r: &mut R) -> PlotOption {
    let chars: Vec<char> = "+x*sSoOtTdDrR".chars().collect();
    r.choose(&chars).map(|c| PointSymbol(*c)).unwrap()
}

fn sizes_iter<'a>(sizes: &'a [Datapoint]) -> Vec<f64> {
    return sizes.iter().map(|r| (r.working_set_size as f64).log2().round()).collect();
}

struct Datapoint {
    working_set_size: usize,
    counter_data: HashMap<String, f64>
}
