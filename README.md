Demo of how working set size affects memory access latency.

This code hops around randomly inside a chunk of memory for a large-ish number of iterations and measures performance counters around that hot loop. As the size of the memory region increases, you can see different performance characteristics kick in as various cache sizes are exceeded.

- Because access to performance counters is generally a root-only thing, you will need to run the binary as root.
- Access to the performance counters is only supported on Linux. If you know a good way access them on other OSs, let me know!
- At the end it will run gnuplot to make a nice chart, so you'll want gnuplot installed.

```
rustup run nightly cargo build --release && sudo ./target/release/memory-access-demo
```
